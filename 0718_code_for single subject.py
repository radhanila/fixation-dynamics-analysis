# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 21:13:48 2018

@author: 贞
"""
#########just test

import csv
import math
import os
import numpy as np
import scipy.stats as st
from matplotlib import pyplot as plt

os.chdir("E:\\Cognitive Science\\Lab rotation\\Lab_AM2\\fig_conditions(345)")

# Path with target eye movement data files
root_path = 'E:\Cognitive Science\Lab rotation\Lab_AM2\Data'

subj = 'AD'
	

def set_fix_params(fix_dict, params):
#===================================================================
#     Setter function to set fixation related parameters into dict
#===================================================================
    t_cnt = params[0]
    fix_dict['ss'][t_cnt].append(params[1]) # saccade size
    fix_dict['sd'][t_cnt].append(params[2]) #  saccade direction
    fix_dict['sdur'][t_cnt].append(params[3]) # saccade duration
    fix_dict['fd'][t_cnt].append(params[4])# fixation duration
    fix_dict['fpos'][t_cnt].append(params[5])# fixation position
    fix_dict['fnum'][t_cnt].append(params[6])# fixation number
    fix_dict['tn'][t_cnt].append(params[7])# trial number
    fix_dict['tgt_flag'][t_cnt].append(params[8])# current fixation is target #T or F
    fix_dict['dtr_flag'][t_cnt].append(params[9])# current fixation distractor #T or F
    fix_dict['chg'][t_cnt].append(params[10])# change or not
    
    return fix_dict


def convert_to_bool(accStr):
#==================================================
#     Function to convert string to boolean value
#==================================================

        bools = {'t':True, 'true': True, '1':True, 'f':False, 'false':False, '0':False}

        if isinstance(accStr, str):
            if accStr.lower() in bools:
                accStr = bools[accStr.lower()]
                return accStr
            else:
                raise ValueError('Invalid literal')
        elif isinstance(accStr, int):
            if accStr in [0,1]:
                return accStr
            else:
                raise ValueError('Invalid literal')
        else:
            raise ValueError('Invalid literal')
    
def read_fix_details(subj):
#===============================================
#       Schematic for fixations and saccades
#
#     ______/\______/\_______
#
#       pF   cS   cF  fS    fF
#
#	pF - preceding fixation
#	cS - current saccades
#	cF - current fixation
#	fS - following saccade
#	fF - following fixation
#===============================================

    fix_dict = {'ss':[], 'sd':[], 'sdur':[], 'fd':[], 'fpos': [], 'fnum':[], 'tn': [], 'recur':[], 'recur_count':[], 'tgt_flag':[], 'dtr_flag':[], 'chg':[], 'tgt_num':[]}
    tn = 0
    t_cnt = -1

    with open(root_path + '\\' +subj + '.dat', 'rb') as csvfile:
        data_reader = csv.reader(csvfile, dialect = 'excel', delimiter = ' ')
        for row in data_reader:
            if int(row[1]) >= 3: # To assess only those trials where at least 3 targets have been presented
                if row[20] != 'NA':    #cFD current fixation duration
                    tgt_num = int(row[1]) 
                    if int(row[0]) > tn:
                        fnum = 0
                        t_cnt += 1                        
                        for key in fix_dict.keys():
                            fix_dict[key].append([])
                    fix_dict['tgt_num'][t_cnt] = tgt_num
                    ss = float(row[11])
                    sd = float(row[18])
                    fd = int(row[20])
                    fpos = eval(row[23])
                    tn = int(row[0])
                    sdur = int(row[12])
                    tgt_flag = convert_to_bool(row[3]) or convert_to_bool(row[5])
                    dtr_flag = convert_to_bool(row[4]) or convert_to_bool(row[6])
                    chg = convert_to_bool(row[9])
                                       
                    fnum += 1
                    params = [t_cnt, ss, sd, sdur, fd, fpos, fnum, tn, tgt_flag, dtr_flag, chg]                    
                    fix_dict = set_fix_params(fix_dict, params)
                        
    fix_dict['tgt_num'][t_cnt] = tgt_num # number of targets
    return fix_dict

    
def delay_plot(array,name,lim):  
##############
# Delay plot #
##############
    curr = []
    prev = []
    for xind, x in enumerate(array):
        if xind > 0:
            curr.append(x)
            prev.append(array[xind - 1])
         
    print st.stats.pearsonr(prev,curr)
    plt.scatter(prev,curr)
    fig = plt.gcf()
    fig.set_size_inches(20, 20)
    plt.title('Delay Plot ('+name+')\n',fontsize=45)
    plt.xlim([0, lim])
    plt.ylim([0, lim])
    plt.xlabel ("Preceding Fixation ("+name+")",fontsize=30)
    plt.ylabel ("Subsequent Fixation ("+name+"+1)\n", fontsize=30)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    fig.savefig(name+' delay_plot.png', dpi=200)
    plt.close()
    
def delay_plot2(array,name,lim):  
##############
# Delay plot #
##############
    curr = []
    prev = []
    for xind, x in enumerate(array):
        if xind > 0:
            curr.append(x)
            prev.append(array[xind - 1])
         
    print st.stats.pearsonr(prev,curr)
    plt.scatter(prev,curr)
    fig = plt.gcf()
    fig.set_size_inches(20, 20)
    plt.title('Delay Plot ('+name+')\n',fontsize=45)
    plt.xlim([0, lim])
    plt.ylim([0, lim])
    plt.xlabel ("Preceding "+name+"",fontsize=30)
    plt.ylabel ("Subsequent "+name+"\n", fontsize=30)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    fig.savefig(name+' delay_plot.png', dpi=200)
    plt.close()


def gen_1st_diff(array):
# =============================================================================
# Generate First difference
# =============================================================================
    diffs = [] 
    for xind, x in enumerate(array):
        if xind > 0:
            diff = x - array[xind - 1]
            diffs.append(diff)
    return diffs


def plot_1st_diff(diffs,name,lim):
# =============================================================================
# Plot First difference
# =============================================================================
    plt.plot(range(len(diffs)), diffs)
    fig = plt.gcf()
    fig.set_size_inches(20, 20)
    plt.title('First Difference ('+name+')\n',fontsize=45)
    plt.xlabel ('Fixation(n)',fontsize=30)
    plt.ylabel ("First Difference ["+name+'+1 - '+name+"]\n", fontsize=30)
    plt.ylim([-lim, lim])
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    fig.savefig(name+' plot_1st_diff.png', dpi=200)
    plt.close()

def plot_1st_diff2(diffs,name,lim):
# =============================================================================
# Plot First difference
# =============================================================================
    plt.plot(range(len(diffs)), diffs)
    fig = plt.gcf()
    fig.set_size_inches(20, 20)
    plt.title('First Difference ('+name+')\n',fontsize=45)
    plt.xlabel ('Fixation(n)',fontsize=30)
    plt.ylabel ("First Difference ["+name+"]\n", fontsize=30)
    plt.ylim([-lim, lim])
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    fig.savefig(name+' plot_1st_diff.png', dpi=200)
    plt.close()
    
def auto_correlation(array,max_lag=100):
    cfs=[]
    for lag in range(1, max_lag+1):
        curr, prev = [], []    
        for xind, x in enumerate(array):
            if xind > lag-1:
                curr.append(x)
                prev.append(array[xind - lag])        
        cf = st.stats.pearsonr(prev,curr)
        cfs.append(cf)
    return cfs

def ac_plot(array,name, max_lag=100):
    cfs=[]
    for lag in range(1, max_lag+1):
        curr, prev = [], []    
        for xind, x in enumerate(array):
            if xind > lag:
                curr.append(x)
                prev.append(array[xind - lag])        
        cf = st.stats.pearsonr(curr, prev)[0]
        cfs.append(cf)
    plt.bar(range(1, max_lag+1),cfs)
    fig = plt.gcf()
    fig.set_size_inches(20, 20)
    plt.title('Auto-Correlation Plot ('+name+')\n',fontsize=45)
    plt.xlabel ('Lag',fontsize=30)
    plt.ylabel ("Correlation Coefficient\n", fontsize=30)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    fig.savefig(name+' ac_plot.png', dpi=200)
    plt.close()
    

def fft_color(array,name,titlesize=45):	

    N = len(array)
    ps = (np.abs(np.fft.fft(array))**2)
    ps = ps[0: N/2]
    freq = np.fft.fftfreq(N)[0:N/2]
    #color noise
    brown = 1/(freq[1:] **2)
    pink = 1/(freq[1:])
    plt.loglog(freq[1:], brown, color = 'brown', lw = 5, label='borwn noise')
    plt.loglog(freq[1:], pink, color = 'pink', lw =5, label='pink noise')
    plt.loglog(freq, ps,  label='data')
 #   plt.xlim([10e-5, 10e7])
 #   plt.ylim([10e-5, 10e7]) 
    plt.legend(fontsize=25) 
    fig = plt.gcf()
    fig.set_size_inches(18, 18)
    plt.title('Comparison with Color Noise ('+name+')\n',fontsize=titlesize)
    plt.xlabel ('Frequency (Log)',fontsize=30)
    plt.ylabel ("Power (Log)\n", fontsize=30)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    fig.savefig(name+' fft_color.png', dpi=200)
    plt.close()

    
#    print min(np.log10(freq))    
#    plt.figure()
#    plt.plot(np.log10(freq), np.log10(ps))
#    plt.scatter(np.log10(freq), np.log10(ps))
#    plt.xlim([10e-5, 10e-1])
#    plt.ylim([10e0, 10e13])
# =============================================================================
#     a1 = random.sample(range(N/2), 1)
#     a2 = random.sample(range(N/2), 1)
#     
#     ps1 = ps[a1]
#     ps2 = ps[a2]
#     
#     f1 = freq[a1]
#     f2 = freq[a2]
# =============================================================================
    
#    np.seterr(all = 'call')
#    print np.log10(ps2/ps1)/np.log10(f2/f1)

def fft_logslop(array):
    N = len(array)
    ps = (np.abs(np.fft.fft(array))**2)
    ps = ps[0: N/2]
    freq = np.fft.fftfreq(N)[0:N/2]
    slope, intercept, r_value, p_value, std_err = st.linregress(np.log10(freq[1:]), np.log10(ps[1:]))
    upper = slope+1.96*std_err
    bottom = slope-1.96*std_err
    return 'slope %.5f'%slope, 'slope interval: %.5f ~ %.5f'%(bottom,upper), 'p_value %.5f'% p_value

def slop_range(array,name,y_1=-2.1,y_2=0.3):
    N = len(array)
    ps = (np.abs(np.fft.fft(array))**2)
    ps = ps[0: N/2]
    freq = np.fft.fftfreq(N)[0:N/2]
    dlts, uppers,bottoms = [],[],[]
    within, idx= [],[]
    for dlt in range(0,len(freq)-10):
        slope = st.linregress(np.log10(freq[1:])[dlt:], np.log10(ps[1:])[dlt:])[0]
        std_err = st.linregress(np.log10(freq[1:])[dlt:], np.log10(ps[1:])[dlt:])[4]
        p_value = st.linregress(np.log10(freq[1:])[dlt:], np.log10(ps[1:])[dlt:])[3]
        if p_value < 0.05:
            upper = slope+1.96*std_err
            bottom = slope-1.96*std_err
            dlts.append(dlt)
            uppers.append(upper)
            bottoms.append(bottom)
            
            if upper>= -1 and bottom <= -1:
                within.append(round(freq[dlt],5))
                idx.append(dlt)
    print within
    plt.vlines(dlts, bottoms, uppers,color= '#1F77B4')
    plt.plot(range(0,N/2),[-1]*(N/2),color='red',lw=3)
#    plt.xlim([10e-5, 10e7])
    plt.title('Log_Log Slop Interval ('+name+')\n',fontsize=45)
    plt.xlabel ('Initial Frequency [freq(n)]',fontsize=30)
    plt.ylabel ("Slop interval\n", fontsize=30)

    plt.xticks(np.arange(0,N/2,1000),(str(freq[0])+'(0)', 
                         str(round(freq[1000],3))+'(1000)',
                         str(round(freq[2000],3))+'(2000)',
                         str(round(freq[3000],3))+'(3000)', 
                         str(round(freq[4000],3))+'(4000)'),fontsize=25)
    plt.yticks(fontsize=25)
    plt.ylim([y_1, y_2])
    fig = plt.gcf()
    fig.set_size_inches(18, 18)
    fig.savefig(name+' slop_range.png', dpi=200)
    plt.close()



print "Subject: " + subj    
fix_dict = read_fix_details(subj)
num_of_trls = len(fix_dict[fix_dict.keys()[0]])

# =============================================================================
# coordinate, x position, y position, and their first difference
# =============================================================================
# extract 
all_fix_pos = [y for x in fix_dict['fpos'] for y in x]
# X
all_x_pos = [pair[0] for pair in all_fix_pos]
x_1st_diff=gen_1st_diff(all_x_pos)

delay_plot(all_x_pos,'X_n', 1600)
plot_1st_diff(x_1st_diff,'X_n',1600)
auto_correlation(all_x_pos)
ac_plot(all_x_pos,'X_n')
fft_color(all_x_pos,'X_n')
fft_logslop(all_x_pos)
slop_range(all_x_pos,'X_n')

# Y
all_y_pos = [pair[1] for pair in all_fix_pos]
y_1st_diff=gen_1st_diff(all_y_pos)

delay_plot(all_y_pos,'Y_n', 1200)
plot_1st_diff(y_1st_diff,'Y_n', 1200)
auto_correlation(all_y_pos)
ac_plot(all_y_pos,'Y_n')
fft_color(all_y_pos,'Y_n')
fft_logslop(all_y_pos)
slop_range(all_y_pos,'Y_n')


# =============================================================================
# extract fixation duration
# =============================================================================
all_fix_dur = [y for x in fix_dict['fd'] for y in x]
dur_1st_diff = gen_1st_diff(all_fix_dur)

delay_plot2(all_fix_dur, 'Fixation Duration',2300)
plot_1st_diff2(dur_1st_diff, 'Fixation Duration',2100)
auto_correlation(all_fix_dur)
ac_plot(all_fix_dur, 'Fixation Duration')
fft_color(all_fix_dur, 'Fixation Duration')
fft_logslop(all_fix_dur)
slop_range(all_fix_dur, 'Fixation Duration')


# =============================================================================
# extract distance (between pre-and-sub fixation)
# =============================================================================
fix_pos = fix_dict['fpos']
all_dist = []
for trial in fix_pos:
    for xind, x in enumerate(trial):
        if xind > 0:
            dist = math.sqrt((x[0] - trial[xind - 1][0])**2+
                             (x[1] - trial[xind - 1][1])**2)
            all_dist.append(dist)
dist_1st_diff = gen_1st_diff(all_dist)    
        
delay_plot2(all_dist,'Distance Between 2 Fixations',1500)
plot_1st_diff2(dist_1st_diff,'Distance Between 2 Fixations',1500)
auto_correlation(all_dist)
ac_plot(all_dist,'Distance Between 2 Fixations')
fft_color(all_dist,'Distance Between 2 Fixations',titlesize=40)
fft_logslop(all_dist)
slop_range(all_dist,'Distance Between 2 Fixations',y_1=-10,y_2=10)

# =============================================================================
# extract distance (between center fixation)
# =============================================================================
all_dist_2_center = []

for xind, x in enumerate(all_fix_pos):
    dist_2_c = math.sqrt((x[0] - 800)**2 + (x[1] - 600)**2)
    all_dist_2_center.append(dist_2_c)
dist_1st_diff = gen_1st_diff(all_dist_2_center)    
        
delay_plot2(all_dist_2_center,'Distance to Center',900)
plot_1st_diff2(dist_1st_diff,'Distance to Center',900)
auto_correlation(all_dist_2_center)
ac_plot(all_dist_2_center,'Distance to Center')
fft_color(all_dist_2_center,'Distance to Center')
fft_logslop(all_dist_2_center)
slop_range(all_dist_2_center,'Distance to Center')

###########09.11#########################   
# =============================================================================
# the horizontal angles
# =============================================================================
def hrzt_func():
    def cal_hrzt(start,end):
        x=end[0]-start[0]
        y=end[1]-start[1]
        h_radian=math.atan2(y,x)
        if h_radian<0:
            h_radian=h_radian+2*np.pi
        return h_radian
    
    fix_pos = fix_dict['fpos']
    h_rdns = []
    for trial_pos in fix_pos:
        trial_h_rdn=[]
        for xind, x in enumerate(trial_pos):
            if xind > 0:
                h_radian=cal_hrzt(trial_pos[xind - 1],x)
                trial_h_rdn.append(h_radian)
        h_rdns.append(trial_h_rdn)
    return h_rdns

h_rdns=hrzt_func() #radians, not angle

# =============================================================================
# polar plot for horizontal angles, covering all trials
# =============================================================================

def polar_plot(h_rdns):

    all_h_rdns=np.hstack(h_rdns).tolist()
    
    n, bins =np.histogram(all_h_rdns,10, range=(0.0,2*np.pi),normed=True)
    n = n*np.diff(bins)

    width = np.pi / 5 #width of the bin
     
    ax = plt.subplot(111, projection='polar')
    bars = ax.bar(bins[0:10].tolist(),n.tolist(), width=width,align='edge')
     
    # Use custom colors and opacity
    for n, bar in zip(n, bars):
        bar.set_facecolor(plt.cm.viridis(n/ .1))#color
        bar.set_alpha(0.4)#opacity


    fig = plt.gcf()
    fig.set_size_inches(18, 18)
    plt.title('Horizontal Angle Polar Plot\n',fontsize=45)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    fig.savefig('horizontal angle polar plot.png', dpi=200)
    plt.close()

polar_plot(h_rdns)


# =============================================================================
#  the dirctional angles
#is defined as the relative angle going from vector r_i to r_i+1
#
#d:dirctional angle; h:horizontal angle
#d_i = h_i+1  -  h_i
# =============================================================================
def d_func(h_rdns):
    d_rdns = []
    for trial_h in h_rdns:
        trial_d_rdn=[]
        for xind, x in enumerate(trial_h):
            if xind > 0:
                d_radian=x-trial_h[xind - 1]
                trial_d_rdn.append(d_radian)
        d_rdns.append(trial_d_rdn)
    return d_rdns

d_rdns=d_func(h_rdns) #radians, not angle

# =============================================================================
# plot histogram
# =============================================================================
def hist_d_rdns(d_rdns):
    #collapes
    all_d_rdns=np.hstack(d_rdns).tolist()

    all_d_angs=[]
    for i in all_d_rdns:
        if i<0:
            i=i+2*np.pi
        ang=math.degrees(i)
        all_d_angs.append(ang)
        
    #plot hist
    n, bins =np.histogram(all_d_angs,36, range=(0.0,360.0),normed=True)
    n = n*np.diff(bins)
    
    plt.bar(bins[0:36].tolist(),n.tolist(),width=10,align='edge',edgecolor='black')
    fig = plt.gcf()
    fig.set_size_inches(18, 15)
    plt.title('Directional Angle Histogram\n',fontsize=45)
    plt.xlabel ('Degree (lag=10deg)',fontsize=30)
    plt.ylabel ("Probability\n", fontsize=30)
    plt.xticks(np.arange(0, 361, step=45),fontsize=25)
    plt.yticks(fontsize=25)
    fig.savefig('directional angle histogram.png', dpi=200)
    plt.close()
    
hist_d_rdns(d_rdns)

    
    
    
    
    
    
    


# # # # # =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================

def extract_subj_fix_pos_details():
#==========================================================================
#	Function to 
#	- get details of fixation position for a subject
#	- generate first difference plots
#	- get correlation coefficient from delay plots
#	- obtain fft and plot psd
#	- perform linear regression to obtain slope of psd from log-log plot
#==========================================================================
    trl_fix_pos_3, trl_fix_pos_4, trl_fix_pos_5 = [], [], []    
    
    for x in range(num_of_trls):   
        if fix_dict['tgt_num_fix'][x] == 3:
            trl_fix_pos_3.append(fix_dict['fpos'][x])
        elif fix_dict['tgt_num_fix'][x] == 4:
            trl_fix_pos_4.append(fix_dict['fpos'][x])
        elif fix_dict['tgt_num_fix'][x] == 5:
            trl_fix_pos_5.append(fix_dict['fpos'][x])        

mast_trl_num_of_fix_3, mast_trl_num_of_fix_4, mast_trl_num_of_fix_5 = [], [], []
mast_fix_dur = []


def extract_all_fix_details(subj):
#======================================================
#	Function 
#	- to get fixation details for all subjects
#	- to get correlation coefficient from delay plot
#======================================================
    print "Subject: "+subj    
    fix_dict = read_fix_details(subj)
    num_of_trls = len(fix_dict[fix_dict.keys()[0]])
        
    fix_dur, trl_num_of_fix = [], []    
    trl_num_of_fix_3, trl_num_of_fix_4, trl_num_of_fix_5 = [], [], []       
    
    for x in range(num_of_trls):
        fix_dur.append(fix_dict['fd'][x])
        trl_num_of_fix.append(len(fix_dict['fd'][x]))
        if fix_dict['tgt_num_fix'][x] == 3:
            trl_num_of_fix_3.append(len(fix_dict['fd'][x]))
        elif fix_dict['tgt_num_fix'][x] == 4:
            trl_num_of_fix_4.append(len(fix_dict['fd'][x]))
        elif fix_dict['tgt_num_fix'][x] == 5:
            trl_num_of_fix_5.append(len(fix_dict['fd'][x]))          
        
    print np.mean(np.hstack(fix_dur))
    print np.sum(trl_num_of_fix), np.mean(trl_num_of_fix)    
    print np.mean(trl_num_of_fix_3), np.mean(trl_num_of_fix_4), np.mean(trl_num_of_fix_5)        
    
    mast_fix_dur.append(np.mean(np.hstack(fix_dur)))
    mast_trl_num_of_fix_3.append(np.mean(trl_num_of_fix_3))
    mast_trl_num_of_fix_4.append(np.mean(trl_num_of_fix_4))
    mast_trl_num_of_fix_5.append(np.mean(trl_num_of_fix_5))
    
    all_trl_fix_dur = [y for x in fix_dur for y in x]
    
    
    print 2/np.sqrt(len(all_trl_fix_dur))
    

            
#    args = np.dstack((mast_trl_num_of_fix_3, mast_trl_num_of_fix_4, mast_trl_num_of_fix_5))[0]
#    print np.shape(args)
#    print np.mean(mast_fix_dur)
#    print np.mean(mast_trl_num_of_fix_3),np.mean(mast_trl_num_of_fix_4), np.mean(mast_trl_num_of_fix_5)    
#    print args
#    f_val, p_val = mne.stats.f_mway_rm(args, [3], correction = True)    
#    print f_val, p_val

    
    
          
