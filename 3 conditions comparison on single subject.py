# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 21:13:48 2018

@author: 贞
"""

import csv
import math
import os
import numpy as np
import scipy.stats as st
from matplotlib import pyplot as plt

os.chdir("E:\\Cognitive Science\\Lab rotation\\Lab_AM2")

# Path with target eye movement data files
root_path = 'E:\\Cognitive Science\\Lab rotation\\Lab_AM2\\Data'

subj = 'EB'

def set_fix_params(fix_dict, params):
#===================================================================
#     Setter function to set fixation related parameters into dict
#===================================================================
    t_cnt = params[0]
    fix_dict['ss'][t_cnt].append(params[1]) # saccade size
    fix_dict['sd'][t_cnt].append(params[2]) #  saccade direction
    fix_dict['sdur'][t_cnt].append(params[3]) # saccade duration
    fix_dict['fd'][t_cnt].append(params[4])# fixation duration
    fix_dict['fpos'][t_cnt].append(params[5])# fixation position
    fix_dict['fnum'][t_cnt].append(params[6])# fixation number
    fix_dict['tn'][t_cnt].append(params[7])# trial number
    fix_dict['tgt_flag'][t_cnt].append(params[8])# current fixation is target #T or F
    fix_dict['dtr_flag'][t_cnt].append(params[9])# current fixation distractor #T or F
    fix_dict['chg'][t_cnt].append(params[10])# change or not
    
    return fix_dict


def convert_to_bool(accStr):
#==================================================
#     Function to convert string to boolean value
#==================================================

        bools = {'t':True, 'true': True, '1':True, 'f':False, 'false':False, '0':False}

        if isinstance(accStr, str):
            if accStr.lower() in bools:
                accStr = bools[accStr.lower()]
                return accStr
            else:
                raise ValueError('Invalid literal')
        elif isinstance(accStr, int):
            if accStr in [0,1]:
                return accStr
            else:
                raise ValueError('Invalid literal')
        else:
            raise ValueError('Invalid literal')
    
def read_fix_details(subj,n):
#===============================================
#       Schematic for fixations and saccades
#
#     ______/\______/\_______
#
#       pF   cS   cF  fS    fF
#
#	pF - preceding fixation
#	cS - current saccades
#	cF - current fixation
#	fS - following saccade
#	fF - following fixation
#===============================================

    fix_dict = {'ss':[], 'sd':[], 'sdur':[], 'fd':[], 'fpos': [], 'fnum':[], 'tn': [], 'recur':[], 'recur_count':[], 'tgt_flag':[], 'dtr_flag':[], 'chg':[], 'tgt_num':[]}
    tn = 0
    t_cnt = -1

    with open(root_path +'\\'+subj+'.dat', 'rb') as csvfile:
        data_reader = csv.reader(csvfile, dialect = 'excel', delimiter = ' ')
        for row in data_reader:
            if int(row[1]) == n: # 
                if row[20] != 'NA':    #cFD current fixation duration
                    tgt_num = int(row[1]) 
                    if int(row[0]) > tn:
                        fnum = 0
                        t_cnt += 1                        
                        for key in fix_dict.keys():
                            fix_dict[key].append([])
                    fix_dict['tgt_num'][t_cnt] = tgt_num
                    ss = float(row[11])
                    sd = float(row[18])
                    fd = int(row[20])
                    fpos = eval(row[23])
                    tn = int(row[0])
                    sdur = int(row[12])
                    tgt_flag = convert_to_bool(row[3]) or convert_to_bool(row[5])
                    dtr_flag = convert_to_bool(row[4]) or convert_to_bool(row[6])
                    chg = convert_to_bool(row[9])
                                       
                    fnum += 1
                    params = [t_cnt, ss, sd, sdur, fd, fpos, fnum, tn, tgt_flag, dtr_flag, chg]                    
                    fix_dict = set_fix_params(fix_dict, params)
                        
    fix_dict['tgt_num'][t_cnt] = tgt_num # number of targets
    return fix_dict

def gen_1st_diff(ipt_dict):
# =============================================================================
# Generate First difference
# =============================================================================
    diffs_dict = {}
    for subj in ipt_dict.keys():
        array = ipt_dict[subj]

        diffs = []
        for xind, x in enumerate(array):
            if xind > 0:
                diff = x - array[xind - 1]
                diffs.append(diff)
        diffs_dict[subj]=diffs
    return diffs_dict

def delay_plot(ipt_dict,name,lim):  
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle('Delay Plot ('+name+')\n',fontsize=100)
    
    tpl=[(k,ipt_dict[k]) for k in sorted(ipt_dict.keys())]
    for idx in range(3):
        array = tpl[idx][1]
        curr = []
        prev = []
        for xind, x in enumerate(array):
            if xind > 0:
                curr.append(x)
                prev.append(array[xind - 1])
        print str(idx+3)+":", st.stats.pearsonr(prev,curr)
        
        axarr[idx].scatter(prev,curr)
        axarr[idx].set_title(str(idx+3)+' Targets',fontsize=50)
        axarr[idx].set_xlim(0,lim)
        axarr[idx].tick_params(labelsize =50)
    
    fig.text(0.5, 0.04, "Preceding Fixation ("+name+")", ha='center',fontsize=100)
    fig.text(0.04, 0.5, "Subsequent Fixation ("+name+"+1)", va='center', rotation='vertical',fontsize=100)

    fig.savefig(name+' delay_plot.png', dpi=96)
    plt.close()
    
    
def delay_plot2(ipt_dict,name,lim):  
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle('Delay Plot ('+name+')\n',fontsize=100)
    
    tpl=[(k,ipt_dict[k]) for k in sorted(ipt_dict.keys())]
    for idx in range(3):
        array = tpl[idx][1]
        curr = []
        prev = []
        for xind, x in enumerate(array):
            if xind > 0:
                curr.append(x)
                prev.append(array[xind - 1])
        print str(idx+3)+":", st.stats.pearsonr(prev,curr)
        
        axarr[idx].scatter(prev,curr)
        axarr[idx].set_title(str(idx+3)+' Targets',fontsize=50)
        axarr[idx].set_xlim(0,lim)
        axarr[idx].tick_params(labelsize =50)
    
    fig.text(0.5, 0.04, "Preceding "+name+"", ha='center',fontsize=100)
    fig.text(0.04, 0.5, "Subsequent "+name, va='center', rotation='vertical',fontsize=100)

    fig.savefig(name+' delay_plot.png', dpi=96)
    plt.close()

def plot_1st_diff(diffs_dict,name,lim):
# =============================================================================
# Plot First difference
# =============================================================================
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle('First Difference ('+name+')\n',fontsize=100)
    
    tpl=[(k,diffs_dict[k]) for k in sorted(diffs_dict.keys())]
    for idx in range(3):
        diffs = tpl[idx][1]
        axarr[idx].plot(range(len(diffs)), diffs)
        axarr[idx].set_title(str(idx+3)+' Targets',fontsize=50)
        axarr[idx].tick_params(labelsize =50)
        axarr[idx].set_ylim(-lim,lim)

    fig.text(0.5, 0.04, 'Fixation(n)', ha='center',fontsize=100)
    fig.text(0.04, 0.5, "First Difference ["+name+'+1 - '+name+"]", va='center', rotation='vertical',fontsize=100)
    fig.savefig(name+' plot_1st_diff.png', dpi=96)
    plt.close()

def plot_1st_diff2(diffs_dict,name,lim):
# =============================================================================
# Plot First difference
# =============================================================================
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle('First Difference ('+name+')\n',fontsize=100)
    
    tpl=[(k,diffs_dict[k]) for k in sorted(diffs_dict.keys())]
    for idx in range(3):
        diffs = tpl[idx][1]
        axarr[idx].plot(range(len(diffs)), diffs)
        axarr[idx].set_title(str(idx+3)+' Targets',fontsize=50)
        axarr[idx].tick_params(labelsize =50)
        axarr[idx].set_ylim(-lim,lim)

    fig.text(0.5, 0.04, 'Fixation(n)', ha='center',fontsize=100)
    fig.text(0.04, 0.5, "First Difference ["+name+"]", va='center', rotation='vertical',fontsize=100)
    fig.savefig(name+' plot_1st_diff.png', dpi=96)
    plt.close()

    
def auto_correlation(ipt_dict,max_lag=100):
    allsbj_cfs={}
    
    for subj in ipt_dict.keys():
        array = ipt_dict[subj]  

        cfs=[]
        for lag in range(1, max_lag+1):
            curr, prev = [], []    
            for xind, x in enumerate(array):
                if xind > lag-1:
                    curr.append(x)
                    prev.append(array[xind - lag])        
            cf = st.stats.pearsonr(prev,curr)
            cfs.append(cf)
        allsbj_cfs[subj]=cfs
    return allsbj_cfs

def pearsonr_ci(x,y,alpha=0.05): 
    ###https://zhiyzuo.github.io/Pearson-Correlation-CI-in-Python/ 
    ''' calculate Pearson correlation along with the confidence interval using scipy and numpy 
  
     Parameters 
      ---------- 
      x, y : iterable object such as a list or np.array 
        Input for correlation calculation 
      alpha : float 
        Significance level. 0.05 by default 
  
      Returns 
      ------- 
     r : float 
        Pearson's correlation coefficient 
      pval : float 
       The corresponding p value 
      lo, hi : float 
        The lower and upper bound of confidence intervals 
      ''' 
    r, p = st.stats.pearsonr(x,y) 
    r_z = np.arctanh(r) 
    se = 1/np.sqrt(len(x)-3) 
    z = st.norm.ppf(1-alpha/2) 
    lo_z, hi_z = r_z-z*se, r_z+z*se 
    lo, hi = np.tanh((lo_z, hi_z)) 
    return r, p, lo, hi 


def ac_plot(ipt_dict,name, max_lag=100):
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle('Auto-Correlation Plot ('+name+')\n',fontsize=100)
    
    tpl=[(k,ipt_dict[k]) for k in sorted(ipt_dict.keys())]
 
    for idx in range(3):
        subj = tpl[idx][0]
        array = tpl[idx][1]
        cfs,lows,highs=[],[], [] 
        for lag in range(1, max_lag+1):
            curr, prev = [], []    
            for xind, x in enumerate(array):
                if xind > lag:
                    curr.append(x)
                    prev.append(array[xind - lag])        
            cf = st.stats.pearsonr(curr, prev)[0]
            low = pearsonr_ci(curr, prev)[2]
            high = pearsonr_ci(curr, prev)[3]
            cfs.append(cf)
            lows.append(low)
            highs.append(high)
        axarr[idx].bar(range(1, max_lag+1),cfs)
        axarr[idx].plot(range(1, max_lag+1),lows,color='r')
        axarr[idx].plot(range(1, max_lag+1),highs,color='orange')
        axarr[idx].set_title(str(subj)+' Targets',fontsize=50)
        axarr[idx].tick_params(labelsize =50)
        
    fig.text(0.5, 0.04, 'Lag', ha='center',fontsize=100)
    fig.text(0.04, 0.5, "Correlation Coefficient", va='center', rotation='vertical',fontsize=100)

    fig.savefig(name+' ac_plot.png', dpi=96)
    plt.close()
    

def fft_color(ipt_dict,name):	
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle('Comparison with Color Noise ('+name+')\n',fontsize=100)
    
    tpl=[(k,ipt_dict[k]) for k in sorted(ipt_dict.keys())]
    for idx in range(3):
        subj = tpl[idx][0]
        array = tpl[idx][1]
        N = len(array)
        ps = (np.abs(np.fft.fft(array))**2)
        ps = ps[0: N/2]
        freq = np.fft.fftfreq(N)[0:N/2]
        #color noise
        brown = 1/(freq[1:] **2)
        pink = 1/(freq[1:])
        
        axarr[idx].loglog(freq, ps,  label='data')
        axarr[idx].loglog(freq[1:], brown, color = 'brown', lw = 5, label='brown noise')
        axarr[idx].loglog(freq[1:], pink, color = 'pink', lw =5, label='pink noise')
        axarr[idx].set_title(str(subj)+' Targets',fontsize=50)
        axarr[idx].tick_params(labelsize =50)
        
    fig.text(0.5, 0.04, 'Frequency (Log)', ha='center',fontsize=100)
    fig.text(0.04, 0.5, "Power (Log)", va='center', rotation='vertical',fontsize=100)

 #   plt.xlim([10e-5, 10e7])
 #   plt.ylim([10e-5, 10e7]) 
    plt.legend(bbox_to_anchor=(-7.5,3.5),borderaxespad = 0.,fontsize=50) 
    fig.savefig(name+' fft_color.png', dpi=96)
    plt.close()

    
#    print min(np.log10(freq))    
#    plt.figure()
#    plt.plot(np.log10(freq), np.log10(ps))
#    plt.scatter(np.log10(freq), np.log10(ps))
#    plt.xlim([10e-5, 10e-1])
#    plt.ylim([10e0, 10e13])
# =============================================================================
#     a1 = random.sample(range(N/2), 1)
#     a2 = random.sample(range(N/2), 1)
#     
#     ps1 = ps[a1]
#     ps2 = ps[a2]
#     
#     f1 = freq[a1]
#     f2 = freq[a2]
# =============================================================================
    
#    np.seterr(all = 'call')
#    print np.log10(ps2/ps1)/np.log10(f2/f1)

def fft_logslope(ipt_dict):
    tpl=[(k,ipt_dict[k]) for k in sorted(ipt_dict.keys())]
    for idx in range(3):
        subj = tpl[idx][0]
        array = tpl[idx][1]
    
        N = len(array)
        ps = (np.abs(np.fft.fft(array))**2)
        ps = ps[0: N/2]
        freq = np.fft.fftfreq(N)[0:N/2]
        slope, intercept, r_value, p_value, std_err = st.linregress(np.log10(freq[1:]), np.log10(ps[1:]))
        upper = slope+1.96*std_err
        bottom = slope-1.96*std_err
        print subj, 'slope %.5f'%slope, 'slope interval: %.5f ~ %.5f'%(bottom,upper), 'p_value %.5f'% p_value
    return 

def slope_range(ipt_dict,name,y_1=-2.5,y_2=0.3):    
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle('Log_Log Slope Interval ('+name+')\n',fontsize=100)
    tpl=[(k,ipt_dict[k]) for k in sorted(ipt_dict.keys())]
    for idx in range(3):
        subj = tpl[idx][0]
        array = tpl[idx][1]
    
        N = len(array)
        ps = (np.abs(np.fft.fft(array))**2)
        ps = ps[0: N/2]
        freq = np.fft.fftfreq(N)[0:N/2]
        dlts, uppers,bottoms = [],[],[]
        within, indx= [],[]
        for dlt in range(0,len(freq)-10):
            slope = st.linregress(np.log10(freq[1:])[dlt:], np.log10(ps[1:])[dlt:])[0]
            std_err = st.linregress(np.log10(freq[1:])[dlt:], np.log10(ps[1:])[dlt:])[4]
            p_value = st.linregress(np.log10(freq[1:])[dlt:], np.log10(ps[1:])[dlt:])[3]
            if p_value < 0.05:
                upper = slope+1.96*std_err
                bottom = slope-1.96*std_err
                dlts.append(dlt)
                uppers.append(upper)
                bottoms.append(bottom)
            
                if upper>= -1 and bottom <= -1:
                    within.append(round(freq[dlt],5))
                    indx.append(dlt)
        #print within
        print subj,'is done.'
        axarr[idx].vlines(dlts, bottoms, uppers,color= '#1F77B4')
        axarr[idx].plot(range(0,N/2),[-1]*(N/2),color='red',lw=3)
        axarr[idx].set_title(str(subj)+' Targets',fontsize=50)
        axarr[idx].tick_params(labelsize =50)
        axarr[idx].set_ylim(y_1, y_2)
        #plt.xlim([10e-5, 10e7])
    
    fig.text(0.5, 0.04, 'Initial Frequency [freq(n)]', ha='center',fontsize=100)
    fig.text(0.04, 0.5, "Slope interval\n", va='center', rotation='vertical',fontsize=100)

# =============================================================================
#     plt.xticks(np.arange(0,N/2,1000),(str(freq[0])+'(0)', 
#                          str(round(freq[1000],3))+'(1000)',
#                          str(round(freq[2000],3))+'(2000)',
#                          str(round(freq[3000],3))+'(3000)', 
#                          str(round(freq[4000],3))+'(4000)'),fontsize=25)
# =============================================================================

    fig.savefig(name+' slope_range.png', dpi=96)
    plt.close()


###########09.11#########################   
# =============================================================================
# the horizontal angles
# =============================================================================
def hrzt_func(fix_dict):
    def cal_hrzt(start,end):
        x=end[0]-start[0]
        y=end[1]-start[1]
        h_radian=math.atan2(y,x)
        if h_radian<0:
            h_radian=h_radian+2*np.pi
        return h_radian
    
    fix_pos = fix_dict['fpos']
    h_rdns = []
    for trial_pos in fix_pos:
        trial_h_rdn=[]
        for xind, x in enumerate(trial_pos):
            if xind > 0:
                h_radian=cal_hrzt(trial_pos[xind - 1],x)
                trial_h_rdn.append(h_radian)
        h_rdns.append(trial_h_rdn)
    return h_rdns

# =============================================================================
# polar plot for horizontal angles, covering all trials
# =============================================================================

def polar_plot(allsbj_h_rdns):
    fig=plt.figure()
    fig.set_size_inches(95, 40)
    fig.suptitle('Horizontal Angle Polar Plot\n',fontsize=100)
    
    tpl=[(k,allsbj_h_rdns[k]) for k in sorted(allsbj_h_rdns.keys())]
    for idx in range(3):
        subj = tpl[idx][0]
        h_rdns = tpl[idx][1]
        all_h_rdns=np.hstack(h_rdns).tolist()
    
        n, bins =np.histogram(all_h_rdns,10, range=(0.0,2*np.pi),normed=True)
        n = n*np.diff(bins)
    
        width = np.pi / 5 #width of the bin
        ax=plt.subplot(1,3,idx+1, projection='polar')
        bars = ax.bar(bins[0:10].tolist(),n.tolist(), width=width,align='edge')
     
        # Use custom colors and opacity
        for n, bar in zip(n, bars):
            bar.set_facecolor(plt.cm.viridis(n/ .1))#color
            bar.set_alpha(0.4)#opacity
        
        ax.set_title(str(subj)+' Targets',fontsize=80)
        ax.tick_params(labelsize = 40)
        ax.set_ylim(0,0.25)

    plt.subplots_adjust(wspace=0.2, hspace =0)#adjust space
    fig.savefig('horizontal angle polar plot.png', dpi=96)
    plt.close()


# =============================================================================
#  the dirctional angles
#is defined as the relative angle going from vector r_i to r_i+1
#
#d:dirctional angle; h:horizontal angle
#d_i = h_i+1  -  h_i
# =============================================================================
def d_func(allsbj_h_rdns):
    allsbj_d_rdns={}
    for subj in allsbj_h_rdns.keys():
        h_rdns = allsbj_h_rdns[subj]
        d_rdns = []
        for trial_h in h_rdns:
            trial_d_rdn=[]
            for xind, x in enumerate(trial_h):
                if xind > 0:
                    d_radian=x-trial_h[xind - 1]
                    trial_d_rdn.append(d_radian)
            d_rdns.append(trial_d_rdn)
        allsbj_d_rdns[subj]=d_rdns
    return allsbj_d_rdns

# =============================================================================
# plot histogram
# =============================================================================
def hist_d_rdns(allsbj_d_rdns):
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle('Directional Angle Histogram\n',fontsize=100)
    
    tpl=[(k,allsbj_d_rdns[k]) for k in sorted(allsbj_d_rdns.keys())]
    for idx in range(3):
        subj = tpl[idx][0]
        d_rdns = tpl[idx][1]
    
        #collapes
        all_d_rdns=np.hstack(d_rdns).tolist()
    
        all_d_angs=[]
        for i in all_d_rdns:
            if i<0:
                i=i+2*np.pi
            ang=math.degrees(i)
            all_d_angs.append(ang)
        
        #plot hist
        n, bins =np.histogram(all_d_angs,36, range=(0.0,360.0),normed=True)
        n = n*np.diff(bins)
    
        axarr[idx].bar(bins[0:36].tolist(),n.tolist(),width=10,align='edge',edgecolor='black')
        axarr[idx].tick_params(labelsize =60)
        axarr[idx].set_title(str(subj)+' Targets',fontsize=50)
        axarr[idx].set_xticks(np.arange(0, 361, step=90))

    fig.text(0.5, 0.04, 'Degree (lag=10deg)',fontsize=100)
    fig.text(0.04, 0.5, "Probability", va='center', rotation='vertical',fontsize=100)

    #plt.xticks(np.arange(0, 361, step=90))

    fig.savefig('directional angle histogram.png', dpi=96)
    plt.close()

# =============================================================================
# bar plot and box_plot for 4 direction
# 
# =============================================================================
def bar_plot_4_drct(allsbj_h_rdns,name,a,b):
    fig, axarr = plt.subplots(1,3, sharex=True, sharey=True)
    fig.set_size_inches(95, 40)
    fig.suptitle(name+' Angle Bar Plot\n',fontsize=100)
    
    tpl=[(k,allsbj_h_rdns[k]) for k in sorted(allsbj_h_rdns.keys())]
    
    for idx in range(3):
        subj = tpl[idx][0]
        h_rdns = tpl[idx][1]
    
        #collapes
        all_h_rdns=np.hstack(h_rdns).tolist()
    
        right,up,left,down =[],[],[],[]
        for i in all_h_rdns:
            if i<0:
                i=i+2*np.pi
            ang=math.degrees(i)
            if (ang >=0 and ang <45) or (ang >= 315 and ang<= 360):
                right.append(ang)
            elif ang >=45 and ang <135:
                up.append(ang)
            elif ang >=135 and ang <225:
                left.append(ang)
            elif ang >=225 and ang <315:
                down.append(ang)
    
        #bar_plot_4_drct
    
        axarr[idx].bar([a,'up',b,'down'],[len(right),len(up),len(left),len(down)],color=['brown', 'blue', 'r','royalblue'])
        axarr[idx].tick_params(labelsize =50)
        axarr[idx].set_title(str(subj)+' Targets',fontsize=50)
    
    fig.text(0.5, 0.04, 'Direction',fontsize=100)
    fig.text(0.04, 0.5, "Quantity", va='center', rotation='vertical',fontsize=100)

    fig.savefig(name+' angle bar plot.png', dpi=96)
    plt.close()


# =============================================================================
# coarse to fine
# fixation duration v.s. ordinal fixation number
# =============================================================================
def transpose(ipt_list): # and also exclude 5% trials containing large ordinal
    trans_list=[]
    num_trl=len(ipt_list)
    for trl in range(num_trl):
        ordinal = 0
        for value in ipt_list[trl]:
            try:
                trans_list[ordinal].append(value)
            except IndexError:
                trans_list.append([value])
            ordinal += 1
    #exclusion
    for ordnl in trans_list:
        if len(ordnl) < num_trl*0.05:
            trans_list.remove(ordnl)
    return trans_list

def m_se(ipt_list):
    m_list, se_list=[],[]
    for ordinal in ipt_list:
        n = len(ordinal)
        mean = np.mean(ordinal)
        std = np.std(ordinal)
        se = std / np.sqrt(n)
        se_list.append(se)
        m_list.append(mean)
    return m_list, se_list

def plot_c2f(ipt_dict,name):
    fig, axarr = plt.subplots(1,3, sharex=False, sharey=False)
    fig.set_size_inches(95, 40)
    fig.suptitle(name,fontsize=100)
    
    tpl=[(k,ipt_dict[k]) for k in sorted(ipt_dict.keys())]
    for idx in range(3):
        subj = tpl[idx][0]
        ipt_list = tpl[idx][1]
        
        m_list, se_list=m_se(transpose(ipt_list))
        axarr[idx].plot(range(1,len(m_list)+1), m_list,color='k')
        axarr[idx].errorbar(range(1,len(m_list)+1), m_list,yerr=se_list, fmt='.',color="g", capsize=5)
        axarr[idx].set_title(str(subj)+' Targets',fontsize=50)
        axarr[idx].tick_params(labelsize =50)
     
    fig.text(0.5, 0.04, "Ordinal number", ha='center',fontsize=100)
    fig.text(0.04, 0.5, "Mean "+name, va='center', rotation='vertical',fontsize=100)

    fig.savefig('coarse to fine '+name+'.png', dpi=96)
    plt.close()
 
# allsbj_fix_dict={}
#allsbj_fix_pos = {}
allsbj_x_pos = {}
allsbj_y_pos = {}
allsbj_fix_dur={}
allsbj_dist_2_center = {}
allsbj_dist={}
allsbj_h_rdns={}
allsbj_fd_wthin_trl={}
allsbj_ss_wthin_trl={}
for n in range(3,6):
    fix_dict = read_fix_details(subj,n)
    # allsbj_fix_dict[subj] = fix_dict
    all_fix_pos = [y for x in fix_dict['fpos'] for y in x]
    # X
    allsbj_x_pos[n]= [pair[0] for pair in all_fix_pos]
    allsbj_x_1st_diff=gen_1st_diff(allsbj_x_pos)
    # Y
    allsbj_y_pos[n] = [pair[1] for pair in all_fix_pos]
    allsbj_y_1st_diff=gen_1st_diff(allsbj_y_pos)
    # extract distance (between center fixation)
    dist_2_c_list=[]
    for xind, x in enumerate(all_fix_pos):
        dist_2_c = math.sqrt((x[0] - 800)**2 + (x[1] - 600)**2)
        dist_2_c_list.append(dist_2_c)
    allsbj_dist_2_center[n]=dist_2_c_list
    allsbj_dist2cntr_1st_diff = gen_1st_diff(allsbj_dist_2_center) 
    # extract distance (between pre-and-sub fixation)
    fix_pos = fix_dict['fpos']
    all_dist = []
    for trial in fix_pos:
        for xind, x in enumerate(trial):
            if xind > 0:
                dist = math.sqrt((x[0] - trial[xind - 1][0])**2+
                                 (x[1] - trial[xind - 1][1])**2)
                all_dist.append(dist)
    allsbj_dist[n]=all_dist
    allsbj_dist_1st_diff = gen_1st_diff(allsbj_dist)  
    # extract fixation duration
    allsbj_fix_dur[n] = [y for x in fix_dict['fd'] for y in x]
    allsbj_dur_1st_diff = gen_1st_diff(allsbj_fix_dur)
    ## the horizontal angles
    #radians, not angle.弧度，不是角度
    allsbj_h_rdns[n]=hrzt_func(fix_dict)
    
    #  the dirctional angles
    allsbj_d_rdns=d_func(allsbj_h_rdns) #radians, not angle #  the dirctional angles
    
    # extract fixation duration with trial inforamtion
    allsbj_fd_wthin_trl[n]=fix_dict['fd']
    
    # extract saccade size with trial inforamtion
    allsbj_ss_wthin_trl[n]=fix_dict['ss']

    
os.chdir("E:\\Cognitive Science\\Lab rotation\\Lab_AM2\\fig_conditions(345)\\"+subj)

########## num_of_trls = len(fix_dict[fix_dict.keys()[0]])

# =============================================================================
# coordinate, x position, y position, and their first difference
# =============================================================================


# X
delay_plot(allsbj_x_pos,'X_n', 1600)
plot_1st_diff(allsbj_x_1st_diff,'X_n',1600)
auto_correlation(allsbj_x_pos)
ac_plot(allsbj_x_pos,'X_n')
fft_color(allsbj_x_pos,'X_n')
fft_logslope(allsbj_x_pos)
slope_range(allsbj_x_pos,'X_n')

# Y
delay_plot(allsbj_y_pos,'Y_n', 1200)
plot_1st_diff(allsbj_y_1st_diff,'Y_n', 1200)
auto_correlation(allsbj_y_pos)
ac_plot(allsbj_y_pos,'Y_n')
fft_color(allsbj_y_pos,'Y_n')
fft_logslope(allsbj_y_pos)
slope_range(allsbj_y_pos,'Y_n')


# =============================================================================
# distance (between center fixation)
# =============================================================================
        
delay_plot2(allsbj_dist_2_center,'Distance to Center',900)
plot_1st_diff2(allsbj_dist2cntr_1st_diff,'Distance to Center',900)
auto_correlation(allsbj_dist_2_center)
ac_plot(allsbj_dist_2_center,'Distance to Center')
fft_color(allsbj_dist_2_center,'Distance to Center')
fft_logslope(allsbj_dist_2_center)
slope_range(allsbj_dist_2_center,'Distance to Center',y_1=-4.1)


# =============================================================================
# extract distance (between pre-and-sub fixation)
# =============================================================================
delay_plot2(allsbj_dist,'Distance Between 2 Fixations',1500)
plot_1st_diff2(allsbj_dist_1st_diff,'Distance Between 2 Fixations',1500)
auto_correlation(allsbj_dist)
ac_plot(allsbj_dist,'Distance Between 2 Fixations')
fft_color(allsbj_dist,'Distance Between 2 Fixations')
fft_logslope(allsbj_dist)
slope_range(allsbj_dist,'Distance Between 2 Fixations')


# =============================================================================
# extract fixation duration
# =============================================================================
delay_plot2(allsbj_fix_dur, 'Fixation Duration',2300)
plot_1st_diff2(allsbj_dur_1st_diff, 'Fixation Duration',2100)
auto_correlation(allsbj_fix_dur)
ac_plot(allsbj_fix_dur, 'Fixation Duration')
fft_color(allsbj_fix_dur, 'Fixation Duration')
fft_logslope(allsbj_fix_dur)
slope_range(allsbj_fix_dur, 'Fixation Duration')

   
    
# =============================================================================
# # polar plot for horizontal angles, covering all trials
# =============================================================================
polar_plot(allsbj_h_rdns) 
bar_plot_4_drct(allsbj_h_rdns, 'Horizontal','right','left')

# histogram dirctional angles
hist_d_rdns(allsbj_d_rdns) 
bar_plot_4_drct(allsbj_d_rdns, 'Directional','same','oppo.')

##################coars to fine
plot_c2f(allsbj_fd_wthin_trl,"Fixation Duration")
plot_c2f(allsbj_ss_wthin_trl,"Saccade Size")
          
