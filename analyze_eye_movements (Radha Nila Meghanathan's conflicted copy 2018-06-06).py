# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 13:13:01 2016

@author: u0092919
"""

import csv
import math
import random
import numpy as np
import scipy.stats as st
from matplotlib import pyplot as plt

# Path with target eye movement data files
root_path = 'C:/Users/radhanila/Dropbox/Lab rotation 2018/Lu/'

subj_list = ['AD']

def main_func():
#========================================
#	Main function for code progression
#========================================
    #extract_subj_fix_pos_details()
	read_fix_details(subj)

def set_fix_params(fix_dict, params):
#===================================================================
#     Setter function to set fixation related parameters into dict
#===================================================================
    t_cnt = params[0]
    fix_dict['ss'][t_cnt].append(params[1])
    fix_dict['sd'][t_cnt].append(params[2])
    fix_dict['sdur'][t_cnt].append(params[3])
    fix_dict['fd'][t_cnt].append(params[4])
    fix_dict['fpos'][t_cnt].append(params[5])
    fix_dict['fnum'][t_cnt].append(params[6])
    fix_dict['tn'][t_cnt].append(params[7])
    fix_dict['tgt'][t_cnt].append(params[8])
    fix_dict['dtr'][t_cnt].append(params[9])
    fix_dict['chg'][t_cnt].append(params[10])
    
    return fix_dict


def convert_to_bool(accStr):
#==================================================
#     Function to convert string to boolean value
#==================================================

        bools = {'t':True, 'true': True, '1':True, 'f':False, 'false':False, '0':False}

        if isinstance(accStr, str):
            if accStr.lower() in bools:
                accStr = bools[accStr.lower()]
                return accStr
        elif isinstance(accStr, int):
            if accStr in [0,1]:
                return accStr
            else:
                raise ValueError('Invalid literal')
        else:
            raise ValueError('Invalid literal')
    
def read_fix_details(subj):
#===============================================
#       Schematic for fixations and saccades
#
#     ______/\______/\_______
#
#       pF   cS   cF  fS    fF
#
#	pF - preceding fixation
#	cS - current saccades
#	cF - current fixation
#	fS - following saccade
#	fF - following fixation
#===============================================

    fix_dict = {'ss':[], 'sd':[], 'sdur':[], 'fd':[], 'fpos': [], 'fnum':[], 'tn': [], 'recur':[], 'recur_count':[], 'tgt':[], 'dtr':[], 'chg':[], 'tgt_num':[]}
    t_no = 0
    t_cnt = -1

    with open(root_path + subj + '.dat', 'rb') as csvfile:
        data_reader = csv.reader(csvfile, dialect = 'excel', delimiter = ' ')
        for row in data_reader:
            if int(row[1]) >= 3: # To assess only those trials where at least 3 targets have been presented
                if row[20] != 'NA':
                    tgt_num = int(row[2])
                    if int(row[0]) > t_no:
                        fnum = 0
                        t_cnt += 1                        
                        for key in fix_dict.keys():
                            fix_dict[key].append([])
                    fix_dict['tgt_num'][t_cnt] = tgt_num
                    ss = float(row[11])
                    sd = float(row[18])
                    cfd = int(row[20])
                    cfpos = eval(row[23])
                    t_no = int(row[0])
                    sdur = int(row[12])
                    tgt_flag = convert_to_bool(row[3]) or convert_to_bool(row[5])
                    dtr_flag = convert_to_bool(row[4]) or convert_to_bool(row[6])
                    chg = convert_to_bool(row[9])
                                       
                    fnum += 1
                    params = [t_cnt, ss, sd, sdur, cfd, cfpos, fnum, t_no, tgt_flag, dtr_flag, chg]                    
                    fix_dict = set_fix_params(fix_dict, params)
                        
    fix_dict['tgt_num'][t_cnt] = tgt_num
    return fix_dict

mast_trl_num_of_fix_3, mast_trl_num_of_fix_4, mast_trl_num_of_fix_5 = [], [], []
mast_fix_dur = []

def extract_all_fix_details():
#======================================================
#	Function 
#	- to get fixation details for all subjects
#	- to get correlation coefficient from delay plot
#======================================================
    for subj in subj_list:
        print subj        
        fix_dict = read_fix_details(subj)
        num_of_trls = len(fix_dict[fix_dict.keys()[0]])
        
        fix_dur, trl_num_of_fix = [], []    
        trl_num_of_fix_3, trl_num_of_fix_4, trl_num_of_fix_5 = [], [], []       
        
        for x in range(num_of_trls):
            fix_dur.append(fix_dict['fd'][x])
            trl_num_of_fix.append(len(fix_dict['fd'][x]))
            if fix_dict['tgt_num'][x] == 3:
                trl_num_of_fix_3.append(len(fix_dict['fd'][x]))
            elif fix_dict['tgt_num'][x] == 4:
                trl_num_of_fix_4.append(len(fix_dict['fd'][x]))
            elif fix_dict['tgt_num'][x] == 5:
                trl_num_of_fix_5.append(len(fix_dict['fd'][x]))          
            
        print np.mean(np.hstack(fix_dur))
        print np.sum(trl_num_of_fix), np.mean(trl_num_of_fix)    
        print np.mean(trl_num_of_fix_3), np.mean(trl_num_of_fix_4), np.mean(trl_num_of_fix_5)        
        
        mast_fix_dur.append(np.mean(np.hstack(fix_dur)))
        mast_trl_num_of_fix_3.append(np.mean(trl_num_of_fix_3))
        mast_trl_num_of_fix_4.append(np.mean(trl_num_of_fix_4))
        mast_trl_num_of_fix_5.append(np.mean(trl_num_of_fix_5))
        
        all_trl_fix_dur = [y for x in fix_dur for y in x]
        
        for lag in range(1, 10):
            curr, prev = [], []    
            for xind, x in enumerate(all_trl_fix_dur):
                if xind > lag:
                    curr.append(x)
                    prev.append(all_trl_fix_dur[xind - lag])        

            print st.stats.pearsonr(curr, prev)
        
        print 2/np.sqrt((len(all_trl_fix_dur)))
        
        # Significance level - two-sided
        lag = 1
        print (-1 + (1.96 * (np.sqrt(len(all_trl_fix_dur) - lag - 1))))/(len(all_trl_fix_dur) - lag)
        print (-1 - (1.96 * (np.sqrt(len(all_trl_fix_dur) - lag - 1))))/(len(all_trl_fix_dur) - lag)
        break
            
#    args = np.dstack((mast_trl_num_of_fix_3, mast_trl_num_of_fix_4, mast_trl_num_of_fix_5))[0]
#    print np.shape(args)
#    print np.mean(mast_fix_dur)
#    print np.mean(mast_trl_num_of_fix_3),np.mean(mast_trl_num_of_fix_4), np.mean(mast_trl_num_of_fix_5)    
#    print args
#    f_val, p_val = mne.stats.f_mway_rm(args, [3], correction = True)    
#    print f_val, p_val

def extract_subj_fix_dur_details():
#======================================================================================
#     Function to get fixation duration details of subject for 3 different conditions
#======================================================================================
    subj = 'AD'    
    print subj
    fix_dict = read_fix_details(subj)
    num_of_trls = len(fix_dict[fix_dict.keys()[0]])
    
    fix_dur, trl_num_of_fix = [], []    
    trl_num_of_fix_3, trl_num_of_fix_4, trl_num_of_fix_5 = [], [], []       
    
    for x in range(num_of_trls):
        fix_dur.append(fix_dict['fd'][x])
        trl_num_of_fix.append(len(fix_dict['fd'][x]))
        if fix_dict['tgt_num'][x] == 3:
            trl_num_of_fix_3.append(len(fix_dict['fd'][x]))
        elif fix_dict['tgt_num'][x] == 4:
            trl_num_of_fix_4.append(len(fix_dict['fd'][x]))
        elif fix_dict['tgt_num'][x] == 5:
            trl_num_of_fix_5.append(len(fix_dict['fd'][x]))

def perform_lin_regress(X, Y):
#============================================
#     Function to perform linear regression
#============================================
    def mean(Xs):
        return sum(Xs) / len(Xs)
    
    m_X = mean(X)
    m_Y = mean(Y)

    def std(Xs, m):
        normalizer = len(Xs) - 1
        return math.sqrt(sum((pow(x - m, 2) for x in Xs)) / normalizer)

    def pearson_r(Xs, Ys):

        sum_xy = 0
        sum_sq_v_x = 0
        sum_sq_v_y = 0

        for (x, y) in zip(Xs, Ys):
            var_x = x - m_X
            var_y = y - m_Y
            sum_xy += var_x * var_y
            sum_sq_v_x += pow(var_x, 2)
            sum_sq_v_y += pow(var_y, 2)            
        return sum_xy / math.sqrt(sum_sq_v_x * sum_sq_v_y)
    # assert np.round(Series(X).corr(Series(Y)), 6) == np.round(pearson_r(X, Y), 6)    
    
    r = pearson_r(X, Y)    

    b = r * (std(Y, m_Y) / std(X, m_X))
    A = m_Y - b * m_X

    return r, b, A
    
    
def extract_subj_fix_pos_details():
#==========================================================================
#	Function to 
#	- get details of fixation position for a subject
#	- generate first difference plots
#	- get correlation coefficient from delay plots
#	- obtain fft and plot psd
#	- perform linear regression to obtain slope of psd from log-log plot
#==========================================================================
    subj = 'AD'
    print subj   
    fix_dict = read_fix_details(subj)
    num_of_trls = len(fix_dict[fix_dict.keys()[0]])
    
    all_fix_pos = []
    trl_fix_pos_3, trl_fix_pos_4, trl_fix_pos_5 = [], [], []    
    
    for x in range(num_of_trls):
        all_fix_pos.append(fix_dict['fpos'][x])    
        if fix_dict['tgt_num'][x] == 3:
            trl_fix_pos_3.append(fix_dict['fpos'][x])
        elif fix_dict['tgt_num'][x] == 4:
            trl_fix_pos_4.append(fix_dict['fpos'][x])
        elif fix_dict['tgt_num'][x] == 5:
            trl_fix_pos_5.append(fix_dict['fpos'][x])        
       
    all_trl_fix_pos = [y for x in all_fix_pos for y in x]
    y_pos = []    
    
	#########################
    # First difference plot #
	#########################
	
    for xind, x in enumerate(all_trl_fix_pos):
        if xind > 0:
            diff = x[1] - all_trl_fix_pos[xind - 1][1]
            y_pos.append(diff)    
    
#    plt.plot(range(len(y_pos)), y_pos)
    y_curr = []
    y_prev = []
    
	##############
    # Delay plot #
	##############
	
    for xind, x in enumerate(all_trl_fix_pos):
        if xind > 0:
            y_curr.append(x[1])
            y_prev.append(all_trl_fix_pos[xind - 1][1])
    print np.corrcoef(y_curr, y_prev)
    print st.stats.pearsonr(y_curr, y_prev)
    return    
    
#    plt.scatter(y_curr, y_prev)    
#    plt.figure()

	#######
	# FFT #
	#######
	
    N = len(y_curr)
    ps = (np.abs(np.fft.fft(y_curr))**2)
    ps = ps[0: N/2]
    freq = np.fft.fftfreq(N)[0:N/2]
    
#    print min(np.log10(freq))    
#    plt.loglog(freq, ps)
##    plt.figure()
##    plt.plot(np.log10(freq), np.log10(ps))
#    plt.xlim([10e-5, 10e-1])
#    plt.ylim([10e0, 10e13])
    a1 = random.sample(range(N/2), 1)
    a2 = random.sample(range(N/2), 1)
    
    ps1 = ps[a1]
    ps2 = ps[a2]
    
    f1 = freq[a1]
    f2 = freq[a2]
    
#    np.seterr(all = 'call')
#    print np.log10(ps2/ps1)/np.log10(f2/f1)
#    
#    brown = 1/(freq[1:] **2)
#    plt.loglog(freq[1:], brown, color = 'brown', lw = 2)
    
#    pink = 1/(freq[1:])
#    plt.loglog(freq[1:], pink, color = 'pink', lw = 2)

	#####################
	# Linear regression #
	#####################
	
    r, b, A = perform_lin_regress(np.log10(freq[1:]), np.log10(ps[1:]))
    print r, b, A
    
    slope, intercept, r_value, p_value, std_err = st.linregress((np.log10(freq[1:]), np.log10(ps[1:])))
    print slope, intercept, r_value, p_value, std_err
#    print len(all_fix_pos) 
#    print num_of_trls

main_func()